<?php

/**
 * @file
 * Contains the administration pages for MORE.
 */

function more_settings_form($form, &$form_state) {

  $form['more_flush'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#value' => 'Click this button regenerate all MORE files once.',
  );

  $form['more_flush']['flush'] = array(
    '#type' => 'submit',
    '#submit' => array('_flush_more'),
    '#value' => 'Flush MORE files',
  );

  $form['more_devel'] = array(
    '#type' => 'checkbox',
    '#title' => t('MORE developer mode'),
    '#description' => t('Enable the developer mode to ensure MORE files are regenerated every page load, regardless of any change done to the MORE file (which may happen when using the @import notation, and changing only the imported file). Note that this setting does not override "Optimize CSS files" if set via <a href="@performance-url">Performance</a>.', array('@performance-url' => url('admin/config/development/performance'))),
    '#default_value' => variable_get('more_devel', FALSE),
  );

  return system_settings_form($form);
}

function _flush_more($form, &$form_state) {

  $more_path = file_default_scheme() . '://more';
  file_unmanaged_delete_recursive($more_path);

  drupal_set_message(t('MORE files flushed.'), 'status');
}
