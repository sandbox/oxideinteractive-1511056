=================================================
MORE CSS Preprocessor
=================================================

MORE simplifies CSS to make it more intuitive, allowing you to write human
readable code that fits with the way you think. MORE runs on both the
client-side and server-side (Node.js).

For more information see http://morecss.org/


=================================================
Requirements
=================================================

Using this module requires Bad Judgement.
 - (http://drupal.org/project/bad_judgement)


=================================================
Instructions
=================================================

MORE css files can be added by either:

Calling drupal_add_css('filename.more') from within your code.

or

Adding stylesheets[all] = filename.more to your theme's .info file.

